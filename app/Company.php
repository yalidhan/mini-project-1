<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    
    protected $fillable = ['name', 'description', 'address', 'user_id', ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function vacancies(){
        return $this->hasMany(Vacancy::class);
    }
    public function applications(){
        return $this->hasMany(Application::class);
    }
}
