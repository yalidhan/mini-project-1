<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;
    protected $fillable = ['account_id', 'vacancy_id', 'company_id'];

    public function account(){
        return $this->belongsTo(Account::class);
    }
    public function vacancy(){
        return $this->belongsTo(Vacancy::class);
    }
    public function company(){
        return $this->belongsTo(Company::class);
    }
}
