<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        // dd(auth()->user()->role(), $role);

        if (!auth::user()) {
            $response = [
                'message' => 'account is invalid',
                'code' => '404'
            ];

            // return response()->json($response, $response['code']);

            return abort(404);

        }

        if ($role == auth()->user()->role()) {
            return $next($request);

        } else {
            return abort(403);
        }

    }
}
