<?php

namespace App\Http\Middleware;

use Closure;

class ProfileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() !== null)
        switch (auth()->user()->role_id) {
            case 0:
                if(! auth()->user()->account)
                    return redirect('/account/create');
                break;
            case 1:
                if(! auth()->user()->company)
                    return redirect('/company/create');
                break;
            default:
                break;
        }
        return $next($request);
    }
}
