<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vacancy;
use App\Company;

class VacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::get();
        return view('vacancy.index', compact('vacancies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::get();
        return view('vacancy.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'], 
            'description' => ['required'], 
            'salary' => ['required'], 
            'contact_person' => ['required'], 
            'company_id' => ['required']
        ]);

        Vacancy::create([
            'title' => $request->title, 
            'description' => $request->description, 
            'salary' => $request->salary, 
            'contact_person' => $request->contact_person, 
            'company_id' => $request->company_id
        ]);

        return redirect()->route('vacancy.create')->with('success', 'Vacancy was created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	$vacancy = Vacancy::find($id);
        $date=$vacancy['created_at']->format('d M Y');
        return view('vacancy.show', compact('vacancy','date'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacancy $vacancy)
    {
        return view('vacancy.edit', compact('vacancy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacancy $vacancy)
    {
        $request->validate([
            'title' => ['required'], 
            'description' => ['required'], 
            'salary' => ['required'], 
            'contact_person' => ['required'], 
            'company_id' => ['required']
        ]);

        $vacancy->update([
            'title' => $request->title, 
            'description' => $request->description, 
            'salary' => $request->salary, 
            'contact_person' => $request->contact_person, 
            'company_id' => $request->company_id
        ]);

        return redirect()->route('vacancy.index')->with('success', 'Vacancy was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacancy $vacancy)
    {
        $vacancy->delete();
        return redirect()->back()->with('success', 'Vacancy was deleted');
        return redirect()->route('vacancy.index')->with('success', 'Vacancy was updated');
    }
    
}
