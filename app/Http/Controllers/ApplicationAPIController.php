<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\Account;

class ApplicationAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = Account::find(auth()->user()->id);
        $applications = $account->applications()->get();

        return $applications;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apply(Request $request)
    {
        $request->validate([
            'vacancy_id' => ['required', 'integer'],
            'company_id' => ['required', 'integer']
        ]);

        $account = Account::find(auth()->user()->id);

        $account->applications()->firstOrCreate([
            'vacancy_id' => $request->vacancy_id,
            'company_id' => $request->company_id
        ]);

        return response()->json('Job was applied', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Application $application)
    {
        $application->delete();
        return response()->json('Application was cancelled', 200);
    }
}
