<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;

class AccountAPIController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'bio' => 'required'
        ]);

        $account = auth()->user()->account()->firstOrCreate([
            'name' => $request->name,
            'bio' => $request->bio
        ]);

        return response()->json('Account was created', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        return $account;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        $request->validate([
            'name' => 'required',
            'bio' => 'required'
        ]);

        $account->update([
            'name' => $request->name,
            'bio' => $request->bio
        ]);

        return response()->json('Account was updated', 200);
    }
    
}
