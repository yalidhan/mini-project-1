<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\SocialUser;
use App\User;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider) 
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Throwable $th) {
            return redirect('/login');
        }

        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);

        return redirect('/home');
    }

    public function findOrCreateUser($socialUser, $provider) 
    {
        $sc = SocialUser::where('provider_id', $socialUser->getId())
                    ->where('provider_name', $provider)
                    ->first();
        if($sc) {
            return $sc->user;
        } else {
            $user = User::where('email', $socialUser->getEmail())->first();

            if(!$user) {
                $user = User::create([
                    'name' => $socialUser->getName(),
                    'email' => $socialUser->getEmail()
                ]);
            }
            
            $user->social_users()->create([
                'provider_id' => $socialUser->getId(),
                'provider_name' => $provider
            ]);

            return $user;
        }
    }
}
