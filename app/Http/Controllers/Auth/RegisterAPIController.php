<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class RegisterAPIController extends Controller
{
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['string','required'],
            'phone_number' => ['alpha_num', 'unique:users'],
            'email' => ['email', 'required', 'unique:users'],
            'password' => ['required', 'min:6']
        ]); 

        User::create([
            'name' => request('name'),
            'phone_number' => request('phone_number'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'role_id' => 0
        ]);

        return response('Terima kasih, anda sudah terdaftar');
    }
}
