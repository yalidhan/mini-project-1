<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vacancy;
use App\Company;

class VacancyAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::get();
        return $vacancies;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'], 
            'description' => ['required'], 
            'salary' => ['required'], 
            'contact_person' => ['required'], 
            'company_id' => ['required']
        ]);

        Vacancy::create([
            'title' => $request->title, 
            'description' => $request->description, 
            'salary' => $request->salary, 
            'contact_person' => $request->contact_person, 
            'company_id' => $request->company_id
        ]);

        return response()->json('Vacancy was created', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vacancy $vacancy)
    {
        return $vacancy;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacancy $vacancy)
    {
        $request->validate([
            'title' => ['required'], 
            'description' => ['required'], 
            'salary' => ['required'], 
            'contact_person' => ['required'], 
            'company_id' => ['required']
        ]);

        $vacancy->update([
            'title' => $request->title, 
            'description' => $request->description, 
            'salary' => $request->salary, 
            'contact_person' => $request->contact_person, 
            'company_id' => $request->company_id
        ]);

        return response()->json('Vacancy was updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacancy $vacancy)
    {
        $vacancy->delete();
        return response()->json('Vacancy was deleted',200);
    }
}
