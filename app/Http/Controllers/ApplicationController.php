<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\Account;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = Account::find(auth()->user()->id);
        $applications = $account->applications()->get();

        return view('account.applications', compact('applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apply(Request $request)
    {
        $request->validate([
            'vacancy_id' => ['required', 'integer'],
            'company_id' => ['required', 'integer']
        ]);

        $account = auth()->user()->account;

        $account->applications()->firstOrCreate([
            'vacancy_id' => $request->vacancy_id,
            'company_id' => $request->company_id
        ]);
        return redirect('/vacancy/'.$request->vacancy_id)->with('success', 'Job was applied');
        // return redirect()->route('vacancy.show/'.$request->vacancy_id)->with('success', 'Job was applied');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Application $application)
    {
        $application->delete();
        return redirect()->back()->with('success', 'Application was cancelled');
    }
}
