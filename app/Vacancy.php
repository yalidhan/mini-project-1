<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacancy extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'description', 'salary', 'contact_person', 'company_id'];
    
    public function company(){
        return $this->belongsTo(Company::class);
    }
    public function applications(){
        return $this->hasMany(Application::class);
    }
}
