@extends('layouts.custom')
@php
    use App\Account;
    use App\Company;
    use App\Vacancy;
@endphp
@section('content')
<!-- container -->
<div class="container">
  <!-- info panel -->
  <div class="row justify-content-center">
    <div class="col-10 info-panel">
      <div class="row">
        <div class="col-lg">
            <img src="{{asset('cover/employee.png')}}" alt="" class="float-left">
            <h4>Pencari Kerja</h4>
            <div class="num"><p>{{Account::all()->count()}}</p></div>
        </div>
        <div class="col-lg">
            <img src="{{asset('cover/company.png')}}" alt="" class="float-left">
            <h4>Perusahaan</h4>
            <div class="num"><p> {{Company::all()->count()}}</p></div>
        </div>
        <div class="col-lg">
            <img src="{{asset('cover/loker.png')}}" alt="" class="float-left">
            <h4>Lowongan</h4>
            <div class="num"><p>{{Vacancy::all()->count()}}</p></div>
        </div>
      </div>
    </div>
  </div>
  <!-- panel end -->
  <!-- Working space -->
    <div class="row mt-5 mb-5 justify-content-center">
      <div class="col-md-12 text-center">
        <h2 class="section-title mb-2">
          Daftar Lowongan Kerja
        </h2>
        @foreach ($vacancies as $vacancy)
        <a href="vacancy/{{$vacancy->id}}" style="text-decoration: none;">
        <ul class="mt-4 list-group list-group-horizontal-md text-left">
          <li class="list-group-item"><img style="height: 150px!important;width:150px!important" src="{{asset('cover/company-logo.png')}}" class="card-img img-fluid" alt="..."></li>
          <li class="list-group-item pt-5 pr-5" style="border-right:0px;"><strong style="font-size: 25px;">{{$vacancy->title}}</strong></br><span style="font-weight:150;color:#ACACAC">{{$vacancy->company->name}}</span></li>
          <li class="list-group-item pt-5 pl-5 pr-5" style="border-right:0px;"><span style="font-size:25px;color:#ACACAC"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$vacancy->company->address}} &nbsp;</span></li>
          <li class="list-group-item pt-5"><span style="font-size: 25px;" class="badge badge-info">{{$vacancy->salary}}</span></li>
        </ul>
        </a>
        @endforeach

      </div>
    </div>
  <!-- working space end -->
</div>
<!-- container end -->
  @endsection