@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color:#e6e6ff">
                    <div class="card-title text-center ">
                    <h5 class="mt-2" 
                    style="color: #3b5998;
                         margin-bottom: 3px;
                         font-weight: bold;
                         font-size: 1.25rem;
                         font-family: Arial, Helvetica, sans-serif;">Profile</h5> 
                    <small class="para">Silahkan masukkan data perusahaan anda.</small>
                    </div>
                </div>      

                <div class="card-body">
                    <form method="POST" action="\company">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <textarea style="border-radius:30px;border: 1px solid #288ebf;height: 150px;resize: none;" id="description" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description" autofocus>
                                </textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea style="border-radius:30px;border: 1px solid #288ebf;height: 150px;resize: none;" id="address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>
                                </textarea>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <center>
                        <div class="form-group row mb-2">
                            
                            <div class="col-md-12">
                                <button type="submit" class="tmbl-grad btn btn-primary btn-lg "
                                style="width:330px;border-radius:30px;
                                background-image: linear-gradient(to right, #314755 0%, #26a0da 51%, #314755 100%)">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
