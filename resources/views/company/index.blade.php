<style>
    .company-card:hover{
        box-shadow: 0 0 3px black;
        cursor: pointer;
    }
</style>

@extends('layouts.custom')
@section('content')
<div class="card">
    <div class="card-header" style="background-color:#e6e6ff">
        <div class="card-title text-center ">
        <h5 class="mt-2" 
        style="color: #3b5998;
             margin-bottom: 3px;
             font-weight: bold;
             font-size: 1.25rem;
             font-family: Arial, Helvetica, sans-serif;">Companies Registered on Cari Kerja</h5>
        </div>
    </div>      

    <div class="card-body text-left">
        @foreach ($companies as $company)
        <div class="card company-card" style="margin: 10px 0;" onclick="window.open('/company/{{$company->id}}', '_self');">
            <div class="card-body">
                <h6>{{$company->name}}</h6>
                <p>{{$company->description}}</p>
            </div>
            <div class="card-footer">
                <span>{{$company->address}}</p>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection