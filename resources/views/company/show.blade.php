@extends('layouts.custom')
@section('content')
@php
    $isUser = false;
    if(auth()->user()->id == $company->user_id){
        $isUser = true;
    }
@endphp
<div class="card">
    <div class="card-header" style="background-color:#e6e6ff">
        <div class="card-title text-center ">
        <h5 class="mt-2" 
        style="color: #3b5998;
             margin-bottom: 3px;
             font-weight: bold;
             font-size: 1.25rem;
             font-family: Arial, Helvetica, sans-serif;">Company Profile</h5>
        </div>
    </div>      

    <div class="card-body">
        <form>
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="name" type="text" class="form-control" value="{{ $company->name }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                <div class="col-md-6">
                    <textarea style="border-radius:30px;border: 1px solid #288ebf;height: 150px;resize: none;" id="description" class="form-control"disabled>{!! $company->description !!}
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                <div class="col-md-6">
                    <textarea style="border-radius:30px;border: 1px solid #288ebf;height: 150px;resize: none;" id="address" class="form-control"disabled>{!! $company->address !!}
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="email" type="text" class="form-control"value="{{ $company->user->email }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="phone_number" type="text" class="form-control"value="{{ $company->user->phone_number }}" disabled>
                </div>
            </div>
        </form>
        <div class="card-header" style="background-color:#e6e6ff">
            <div class="card-title text-center ">
                <h5 class="mt-2" 
                    style="color: #3b5998;
                    margin-bottom: 3px;
                    font-weight: bold;
                    font-size: 1.25rem;
                    font-family: Arial, Helvetica, sans-serif;">Vacancies
                </h5>
            </div>
        </div> 
        <div class="card-body">
            @if ($isUser)
            <a href="\vacancy\create" class="btn btn-primary float-right" style="margin:-10px 0 10px">Add New Vacancy
            </a>
            @endif
            <table class="table table-bordered table-striped">
                <tr><th>No</th><th>Job Title</th><th>Time</th>@if($isUser)<th>Action</th> @endif </tr>
                @foreach ($company->vacancies as $key=>$vacancy)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><a href="/vacancy/{{$vacancy->id}}">{{$vacancy->title}}</a></td>
                        <td>{{$vacancy->created_at}}</td>
                        @if($isUser)
                        <td><a onclick="event.preventDefault(); document.getElementById('delete-form-{{$vacancy->id}}').submit();"
                                class="btn btn-warning">Delete
                            </a>
                            <form id="delete-form-{{$vacancy->id}}" action="/vacancy/{{$vacancy->id}}" method="post" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection