@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"style="background-color:#e6e6ff">
                    <div class="card-title text-center ">
                    <h5 class="mt-2" 
                    style="color: #3b5998;
                         margin-bottom: 3px;
                         font-weight: bold;
                         font-size: 1.25rem;
                         font-family: Arial, Helvetica, sans-serif;">HALLO, YANG DI SANA</h5> 
                    <small class="para">Silahkan daftar atau gunakan akun media sosial kamu.</small>
                    </div>
                 </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" autocomplete="phone_number">

                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('User Type') }}</label>

                            <div class="col-md-6">
                                <select style="border-radius:30px;border: 1px solid #288ebf;" id="role_id" class="form-control @error('role_id') is-invalid @enderror" name="role_id" value="{{ old('role_id') }}" required autocomplete="role_id">
                                    <option value="0" selected>Personal</option>
                                    <option value="1">Company</option>
                                </select>

                                @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <center>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-lg"  style="width:330px;border-radius:30px;
                                background-image: linear-gradient(to right, #314755 0%, #26a0da 51%, #314755 100%)">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        
                        <br>
                        <!-- <center> -->
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                            <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook" 
                            style="background-color: #3B5998;
                                color: white;">
                                <i class="fa fa-facebook"></i>
                                Login with facebook
                            </a>
                            <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter"
                            style="background-color: #55ACEE;color: white">
                                <i class="fa fa-twitter"></i>
                                Login with Twitter
                            </a>
                            </div>
                        </div>
                        </center>

    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
