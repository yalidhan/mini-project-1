@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color:#e6e6ff">
                    <div class="card-title text-center ">
                    <h5 class="mt-2" 
                    style="color: #3b5998;
                         margin-bottom: 3px;
                         font-weight: bold;
                         font-size: 1.25rem;
                         font-family: Arial, Helvetica, sans-serif;">HALLO, YANG DI SANA</h5> 
                    <small class="para">Silahkan masuk dengan akun web ini atau dengan akun media sosial kamu.</small>
                    </div>
                </div>      

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <center>
                        <div class="form-group row mb-2">
                            
                            <div class="col-md-12">
                                <button type="submit" class="tmbl-grad btn btn-primary btn-lg "
                                style="width:330px;border-radius:30px;
                                background-image: linear-gradient(to right, #314755 0%, #26a0da 51%, #314755 100%)">
                                    {{ __('Login') }}
                                </button>
                                @if (Route::has('password.request'))
                                </br><a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                            <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook" 
                            style="background-color: #3B5998;
                                color: white;">
                                <i class="fa fa-facebook"></i>
                                Login with facebook
                            </a>
                            <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter"
                            style="background-color: #55ACEE;color: white">
                                <i class="fa fa-twitter"></i>
                                Login with Twitter
                            </a>
                            </div>
                        </div>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
