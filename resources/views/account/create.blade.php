@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color:#e6e6ff">
                    <div class="card-title text-center ">
                    <h5 class="mt-2" 
                    style="color: #3b5998;
                         margin-bottom: 3px;
                         font-weight: bold;
                         font-size: 1.25rem;
                         font-family: Arial, Helvetica, sans-serif;">Profile</h5> 
                    <small class="para">Silahkan masukkan data diri anda.</small>
                    </div>
                </div>      

                <div class="card-body">
                    <form method="POST" action="\account">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                            <div class="col-md-6">
                                <input style="border-radius:30px;border: 1px solid #288ebf;" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Biodata') }}</label>

                            <div class="col-md-6">
                                <textarea style="border-radius:30px;border: 1px solid #288ebf;height: 150px;resize: none;" id="bio" class="form-control @error('bio') is-invalid @enderror" name="bio" value="{{ old('bio') }}" required autocomplete="bio" autofocus>
                                </textarea>

                                @error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <center>
                        <div class="form-group row mb-2">
                            
                            <div class="col-md-12">
                                <button type="submit" class="tmbl-grad btn btn-primary btn-lg "
                                style="width:330px;border-radius:30px;
                                background-image: linear-gradient(to right, #314755 0%, #26a0da 51%, #314755 100%)">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
