@extends('layouts.custom')
@section('content')
<div class="card">
    <div class="card-header" style="background-color:#e6e6ff">
        <div class="card-title text-center ">
        <h5 class="mt-2"
        style="color: #3b5998;
             margin-bottom: 3px;
             font-weight: bold;
             font-size: 1.25rem;
             font-family: Arial, Helvetica, sans-serif;">Profile</h5>
        </div>
    </div>

    <div class="card-body">
        <form>
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="name" type="text" class="form-control" value="{{ $account->name }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="uname" class="col-md-4 col-form-label text-md-right">{{ __('User Name') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="uname" type="text" class="form-control"value="{{ $account->user->name }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="email" type="text" class="form-control"value="{{ $account->user->email }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                <div class="col-md-6">
                    <input style="border-radius:30px;border: 1px solid #288ebf;" id="phone_number" type="text" class="form-control"value="{{ $account->user->phone_number }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Biodata') }}</label>

                <div class="col-md-6">
                    <textarea style="border-radius:30px;border: 1px solid #288ebf;height: 150px;resize: none;" id="bio" class="form-control"disabled>{!! $account->bio !!}
                    </textarea>
                </div>
            </div>
        </form>
        @if (auth()->user()->id == $account->user_id && $account->applications->count() > 0)
        <div class="card-header" style="background-color:#e6e6ff">
            <div class="card-title text-center ">
                <h5 class="mt-2"
                    style="color: #3b5998;
                    margin-bottom: 3px;
                    font-weight: bold;
                    font-size: 1.25rem;
                    font-family: Arial, Helvetica, sans-serif;">Your Applications
                </h5>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tr><th>No</th><th>Job Title</th><th>Company</th><th>Time</th><th>Action</th></tr>
                @foreach ($account->applications as $key=>$application)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><a href="/vacancy/{{$application->vacancy->id}}">{{$application->vacancy->title}}</a></td>
                        <td><a href="/company/{{$application->company->id}}">{{$application->company->name}}</a></td>
                        <td>{{$application->created_at}}</td>
                        <td><a onclick="event.preventDefault(); document.getElementById('delete-form-{{$application->id}}').submit();"
                                class="btn btn-warning">Cancel
                            </a>
                            <form id="delete-form-{{$application->id}}" action="/application/{{$application->id}}" method="POST" style="display: none;">
                                @csrf
                                @method("DELETE")
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        @endif
    </div>
</div>
@endsection
