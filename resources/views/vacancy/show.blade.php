@extends('layouts.custom')
@section('content')
    <!-- container -->
<div class="container">
  <!-- info panel -->
  <div class="row justify-content-center">
    <div class="col-10 info-panel">
      <div class="row">
            <h3>{{$vacancy->title}}</h3>
            <p>{{$vacancy->salary}} / Bulan</p>
      </div>
    </div>
  </div>
  <!-- panel end -->
  <!-- Working space -->
    <div class="row vacshow-top">
        <div class="col-md-12 mt-5 pt-5">
            <!-- card -->
            <div class="card mb-3" style="max-width: 650px;border:0px">
                <div class="row no-gutters">
                    <div class="col-md-4">
                    <img width="150px" height="150px" src="{{asset('cover/company-logo.png')}}" class="card-img img-fluid" alt="...">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title mt-5">{{$vacancy->title}}</h5>
                        <a href="/company/{{$vacancy->company->id}}"><span><i class="fa fa-briefcase" aria-hidden="true"></i> {{$vacancy->company->name}} &nbsp;</span></a> <span><i class="fa fa-map-marker" aria-hidden="true"></i> {{$vacancy->company->address}} &nbsp;</span>
                    </div>
                    </div>
                </div>
                </div>
                </div>
            <!-- end card -->
        </div>
                    <!-- gambar -->
        <div class="row">
             <div class="col-md-7 mt-5">
                 <img height="980px" width="580px"src="{{asset('cover/workingspace.png')}}" class="img-fluid" alt="">
                <div class="mt-5 text-left">
                <h4 style="color:#89ba16!important;"><i class="fa fa-align-left" aria-hidden="true"> Deskripsi Pekerjaan</i></h4>
                <p>{{$vacancy->description}}</p>
            </div>
             </div>
             <div class="col-md-5 mt-5">
                 <div class="bg-light p-3 border-rounded mb-4 text-left">
                     <h3 style="color:#89ba16!important"class="text-primary  mt-3 h5 pl-3 mb-3"><strong>Rangkuman Pekerjaan</strong></h3>
                    <ul class="list-unstyled pl-3 mb-0">
                        <li class="mb-2"><strong class="text-black">Dipublish pada : </strong> {{$date}}</li>
                        <li class="mb-2"><strong class="text-black">Estimasi Gaji : </strong> {{$vacancy->salary}}</li>
                        <li class="mb-2"><strong class="text-black">Perusahaan : </strong> {{$vacancy->company->name}}</li>
                        <li class="mb-2"><strong class="text-black">Kontak Person : </strong> {{$vacancy->contact_person}}</li>
                        <li class="mb-2"><strong class="text-black">Lokasi Pekerjaan : </strong> {{$vacancy->company->address}}</li>
                    </ul>
                 </div>
             </div>
          </div>
            <!-- end gambar -->
  <!-- working space end -->
  @if (auth()->user()->role_id==0)
  <form action="/application/apply" method="POST">
    @csrf
    <input type="hidden" value="{{$vacancy->id}}" name="vacancy_id">
    <input type="hidden" value="{{$vacancy->company_id}}" name="company_id">
  <button type="submit"class="btn btn-success" style="background-color:#89ba16!important;color:cornsilk;border-radius:30px;">Masukkan Lamaran</button>
  @endif
</form>
@if (\Session::has('success'))
    <div class="alert alert-success mt-5">
             <p>Berhasil memasukkan lamaran</p>
    </div>
@endif
</div>
@if (auth()->user()->id== $vacancy->company->user_id)

<div class="card-header" style="background-color:#e6e6ff">
  <div class="card-title text-center ">
      <h5 class="mt-2" 
          style="color: #3b5998;
          margin-bottom: 3px;
          font-weight: bold;
          font-size: 1.25rem;
          font-family: Arial, Helvetica, sans-serif;">Applications
      </h5>
  </div>
</div> 
<div class="card-body">
  <table class="table table-bordered table-striped">
      <tr><th>No</th><th>Name</th><th>Time</th></tr>
      @foreach ($vacancy->applications as $key=>$application)
          <tr>
              <td>{{$key+1}}</td>
              <td><a href="/account/{{$application->account_id}}">{{$application->account->name}}</a></td>
              <td>{{$application->created_at}}</td>
          </tr>
      @endforeach
  </table>
</div>
@endif
<!-- container end -->
@endsection