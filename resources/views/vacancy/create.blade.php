@extends('layouts.custom')
@section('content')
<!-- Info Panel -->
<div class="row justify-content-center">
    <div class="col-10 info-panel text-center">
        <h2>Buat Lamaran Pekerjaan</h2>
    </div>
  </div>
<!-- Info Panel End -->
<!-- Form -->
<div class="row mt-5 justify-content-center">
    <div class="col-md-6 pt-3 pb-3" style="border:solid 1px #ccc!important;border-radius:15px;">
        <form class="text-left" action="/vacancy" method="POST">
            @csrf
            <input type="hidden" name="company_id" value="{{auth()->user()->company->id}}">
        <div class="form-group">
            <label for="title">Judul Lowongan</label>
            <input type="text" class="form-control" name="title" id="title">
        </div>
        <div class="form-group">
            <label for="description">Deskripsi Pekerjaan</label>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="salary">Gaji</label>
            <input type="text" class="form-control" name="salary" id="salary">
        </div>
        <div class="form-group">
            <label for="contact_person">Kontak Person</label>
            <input type="text" class="form-control" name="contact_person" id="salary">
        </div>
        <center>
        <button type="submit" class="btn btn-success" style="background-color:#89ba16!important;color:cornsilk;border-radius:30px;">Buat Lowongan Kerja</button>
        </center>    
        </form>
    </div>
</div>
@if (\Session::has('success'))
    <div class="alert alert-success mt-5">
        <p>Berhasil membuat lowongan pekerjaan</p>
    </div>
@endif
<!-- End form -->
@endsection