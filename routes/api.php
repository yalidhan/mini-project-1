<?php

use Illuminate\Foundation\Console\RouteListCommand;
use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function(){

    Route::post('register', 'RegisterAPIController');
    Route::post('login', 'LoginAPIController');
//    Route::post('logout', 'LogoutController');
});

// Route For Account
// Route::resource('account', 'AccountAPIController')->only(['store', 'update', 'show']);
Route::post('account','AccountAPIController@store')->name('account.store');
Route::put('account/{id}','AccountAPIController@update')->name('account.update');
Route::get('account/{id}','AccountAPIController@show')->name('account.show');


// Route For Company
// Route::resource('company', 'CompanyAPIController')->only(['index','store', 'update', 'show', 'destroy']);
Route::get('company','CompanyAPIController@index');
Route::post('company','CompanyAPIController@store')->name('company.store');
Route::put('company/{id}','CompanyAPIController@update')->name('company.update');
Route::delete('company/{id}','CompanyAPIController@destroy')->name('company.destroy');

// Route For Company
// Route::resource('vacancy', 'VacancyAPIController')->only(['index','store', 'update', 'show', 'destroy']);
Route::get('vacancy','VacancyAPIController@index')->name('vacancy.index');
Route::post('vacancy','VacancyAPIController@store')->name('vacancy.store');
Route::put('vacancy','VacancyAPIController@update')->name('vacancy.update');
Route::delete('vacancy','VacancyAPIController@destroy')->name('vacancy.destroy');



// Route for applications
Route::get('application' , 'ApplicationAPIController@index')->name('application.index');
Route::post('application/apply' , 'ApplicationAPIController@apply')->name('application.apply');
Route::delete('application/{application}' , 'ApplicationAPIController@cancel')->name('application.cancel');
