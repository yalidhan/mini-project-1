<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', 'HomeController@index')->name('home')->middleware('profile');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('profile');

Route::get('auth/{provider}', 'Auth\SocialiteController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\SocialiteController@handleProviderCallback');

// Route for Account's CRUD
// Route::resource('account', 'AccountController')->only(['create', 'store', 'edit', 'update', 'show']);
Route::get('account/create','AccountController@create')->name('account.create')->middleware('checkRole:personal');
Route::post('account','AccountController@store')->name('account.store')->middleware('checkRole:personal');
Route::get('account/{id}/edit','AccountController@edit')->name('account.edit')->middleware('checkRole:personal');
Route::put('account/{id}','AccountController@update')->name('account.update')->middleware('checkRole:personal');
Route::get('account/{account}','AccountController@show')->name('account.show');


// Route For Company
Route::get('company','CompanyController@index')->name('company.index');
Route::get('company/create','CompanyController@create')->name('company.create')->middleware('checkRole:company');
Route::post('company','CompanyController@store')->name('company.store')->middleware('checkRole:company');
Route::get('company/{id}','CompanyController@show')->name('company.show');
Route::get('company/{id}/edit','CompanyController@edit')->name('company.edit')->middleware('checkRole:company');
Route::put('company/{id}','CompanyController@update')->name('company.update')->middleware('checkRole:company');
Route::delete('company/{id}','CompanyController@destroy')->name('company.destroy')->middleware('checkRole:company');

// Route For Company
Route::get('vacancy','VacancyController@index')->name('vacancy.index');
Route::get('vacancy/create','VacancyController@create')->name('vacancy.create')->middleware('checkRole:company');
Route::get('vacancy/{id}','VacancyController@show')->name('vacancy.show');
Route::get('vacancy/{id}/edit','VacancyController@edit')->name('vacancy.edit')->middleware('checkRole:company');
Route::post('vacancy','VacancyController@store')->name('vacancy.store')->middleware('checkRole:company');
Route::put('vacancy','VacancyController@update')->name('vacancy.update')->middleware('checkRole:company');
Route::delete('vacancy','VacancyController@destroy')->name('vacancy.destroy')->middleware('checkRole:company');


// Route for applications
Route::get('/application' , 'ApplicationController@index')->name('application.index');
Route::post('/application/apply' , 'ApplicationController@apply')->name('application.apply')->middleware('checkRole:personal');;
Route::delete('/application/{application}' , 'ApplicationController@cancel')->name('application.cancel')->middleware('checkRole:personal');;
